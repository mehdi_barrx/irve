######################################################################################################
#############################            BORNES IRVE  2021          ##################################
######################################################################################################
library(tidyverse)
library(openxlsx)
library(rlang)
library(lubridate)
library(zipR)
library(sf)
library(cartography)

#irve87ddt <- st_read(dsn = "S:/00_commun_micat/3_observation_Statistiques/d_etudes/energie_climat/Bornes irve/irve/data", layer = "l_bornes_irve_p_087")
com20 <- st_read(dsn = "data", layer = "COMMUNE")
com20 <- st_transform(com20, 4326)
base2021 <- read.csv("data/bornes-irve-20210120.csv",header = T, sep = ";", encoding = "UTF-8")%>% 
  mutate(dep = str_sub(code_insee, 1, 2), .before = 7) %>% 
  mutate(n_amenageur = ifelse(n_amenageur == "S‚olis", yes = "Séolis", no = n_amenageur)) %>% 
  mutate(n_operateur = ifelse(n_operateur == "S‚olis", yes = "Séolis", no = n_operateur))

base2021[base2021 == "" ] <- NA   # Introduction de NA à la place des cellules vides

basesanscode <- base2021 %>% # Création du jeu filtré à retravailler
  filter(is.na(dep))

base2021 <- base2021 %>% # Création du jeu filtré propre à fusionner avec le jeu traités
  filter(!is.na(dep))

#Récupération des obs en NA sans code
basesanscode <- basesanscode %>% 
  filter(id_station %in% c("FR*G52*P24256*001", "FR*G52*P24037*001", "FR*G52*P33424*001", "FR*G52*P79061*001")
         | n_amenageur %in% c("BE TROM", "Séolis", "S‚olis")) 

# Introduction des code insee manquants
basesanscode <- basesanscode %>% 
  mutate(code_insee = ifelse(id_station == "FR*G52*P24256*001", yes = "24256",
                 no = ifelse(id_station == "FR*G52*P24037*001", yes = "24037",
                 no = ifelse(id_station == "FR*G52*P33424*001", yes = "33424",
                 no = ifelse(id_station == "FR*G52*P79061*001", yes = "79061",
                 no = ifelse(id_station == "FR*G38*P33550*001", yes = "33550",
                 no = ifelse(id_station == "FRSEOPAB64023A", yes = "79018",
                 no = ifelse(id_station == "FRSEOPAB08004A", yes = "79038",
                 no = ifelse(id_station == "FRSEOPAB20087A", yes = "79056",
                 no = ifelse(id_station == "FRSEOPAB50103A", yes = "79081",
                 no = ifelse(id_station %in% c("FRSEOPAB33001A", "FRSEOPAB31300A"), yes = "79080",
                 no = ifelse(id_station == "FRSEOPAB12008A", yes = "79116",
                 no = ifelse(id_station == "FRSEOPAB47092A", yes = "79293",
                 no = ifelse(id_station == "FRSEOPAB26031A", yes = "79299",
                 no = ifelse(id_station == "FRSEOPAB01041A", yes = "79351",
                 no = ifelse(id_station == "FRSEOPAB12201A", yes = "79049",
                 no = code_insee))))))))))))))))

# Récupération des codes dep manquants
basesanscode <- basesanscode %>% 
  mutate(dep = str_sub(code_insee, 1, 2))

# Réunion des deux jeux de données propres
base2021 <- bind_rows(base2021, basesanscode)

# Base temporaire Nouvelle Aquitaine
base2021_NA <- base2021 %>% 
  filter(dep %in% c("16", "17", "19", "23", "24", "33", "40", "47", "64", "79", "86", "87")) %>% 
  mutate(n_amenageur = ifelse(id_station %in% c("FR-S64-E64024-001", "FR-S64-E64024-006", "FR-S64-E64024-003"), yes = "SDEPA", no = n_amenageur),
         n_operateur= ifelse(id_station %in% c("FR-S64-E64024-001", "FR-S64-E64024-006", "FR-S64-E64024-003"), yes = "BYES", no = n_operateur))

base2021_NA <- base2021_NA %>% 
  mutate(n_station = str_replace(string = n_station, pattern = "[‚]", replacement = "é"))

# à poursuivre
######################################################################################################
################### TRAITEMENTS GEOGRAPHIQUES POUR RECUPERER LES CODES INSEE MANQUANTS ###############
### Gestion des problemes de latitude et longitude ###
# A revoir, problème : introduction de NA lors de la conversion en numeric
# irve <- irve %>% 
#   mutate(Ylatitude = str_replace(string = Ylatitude, pattern = "[*]", replacement = "."),
#          Xlongitude = str_replace(string = Xlongitude, pattern = "[*]", replacement = "."),
#          Ylatitude = as.numeric(Ylatitude),
#          Xlongitude = as.numeric(Xlongitude))
# 
# base2021_ig <- base2021_NA %>% 
#   mutate(Ylatitude = as.numeric(Ylatitude),
#          Xlongitude = as.numeric(Xlongitude))
# 
# irve_geo <- filter(base2021_ig, is.na(Xlongitude))
# irve_geo2 <- filter(base2021_ig, is.na(Ylatitude))
### Test des code_insee manquants, transformation du csv en shp, jointure spatiale, récupération code_insee ###

# irve_geo <- filter(irve, is.na(code_insee))
# 
# irve_geo <- st_as_sf(irve_geo, coords = c("Xlongitude", "Ylatitude"), crs = 4326, agr = "constant")
# irve_geo <- st_join(irve_geo, com20, join = st_within, left = TRUE) %>% 
#   mutate(code_insee = INSEE_COM) %>% 
#   select(-ID, -NOM_COM, -NOM_COM_M, -INSEE_COM, -STATUT, -INSEE_CAN,
#          -INSEE_ARR, -INSEE_DEP, -CODE_EPCI, -POPULATION, - TYPE) %>% 
#   filter(INSEE_REG == 75)

# Si irve_geo est vide OK, sinon vérifier les enregistrements avant jointure spatiale avec irve #

### Gestion des codes insee faux + maj cog20 ###

# irve <- st_as_sf(irve, coords = c("Xlongitude", "Ylatitude"), crs = 4326, agr = "constant")
# irve <- st_join(irve, com20, join = st_within, left = TRUE) %>% 
#   mutate(code_insee = ifelse(is.na(code_insee), yes = INSEE_COM, no = code_insee),
#          erreur_geo = INSEE_COM == code_insee,
#          code_insee = ifelse(erreur_geo == FALSE, yes = INSEE_COM, no = code_insee)) %>%
#   select(-ID, -NOM_COM_M, -STATUT, -INSEE_CAN, -INSEE_ARR, - INSEE_COM, - TYPE, - erreur_geo)
# 
# st_geometry(irve) <- NULL # Suppression de la géométrie pour faciliter les traitements avec dplyr
# st_geometry(com20) <- NULL
# pb_geo <- filter(irve, erreur_geo == F,
#                  INSEE_REG == 75) %>% 
#   arrange(code_insee)
# 
# 
# com_fe <- com20
# st_geometry(com_fe) <- NULL
# 
# ###############################################################################################
# ### Gestion des stations sans id ###
# irve <- irve %>% 
#   mutate(id_station = ifelse(n_amenageur %in% c("SDE 23", "SIEIL37", "CGLE"), yes = "sans_id_station", no = id_station))
# 
# ###############################################################################################
# ### Gestion des stations sans noms ###
# irve <- irve %>% 
#   mutate(n_station = ifelse(n_amenageur == "SIEGE 27", yes = "Gaillon", no = n_station))
  